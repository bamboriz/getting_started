
//Math - Average of a set
const average = (...nums) => [...nums].reduce((acc, val) => acc + val, 0) / nums.length;

let ans = average(...[1,2,3,4,5,6]);
console.log(ans);

//Math - Closest number
const clampNumber = (num, a, b) => Math.max(Math.min(num, Math.max(a, b)), Math.min(a, b));

// Math - COmbinations
const factorial = n =>
  n < 0
    ? (() => {
        throw new TypeError('Negative numbers are not allowed!');
      })()
    : n <= 1
      ? 1
      : n * factorial(n - 1);

const Comb = (n,r) => factorial(n)/(factorial(r) * factorial(n - r));

console.log(Comb(15,4)) //1365


//Math - isDivisible
const isDivisible = (dividend, divisor) => dividend % divisor === 0;

const fnx = x => {
  if(isDivisible(x,10)){
    return x;
  }else{
    return ( Math.round(x/10)*10);
  }
};
ans = [10,12,25,52,78].map(fnx);
console.log(ans); //[10, 10, 30, 50, 80]

//Math - randomIntegerInRange 

const randomIntegerInRange = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

// Random binary array of 10 numbers
let binary = (len)=> {
  let rep = [];
  for(let i = 0; i<len; i++){
    rep.push(randomIntegerInRange(0, 1));
  }
  return rep;
};
console.log(binary(10)); //[0, 0, 1, 1, 1, 1, 1, 1, 0, 1]

